## Draw circles inside a donut

## Examples:

### 100 circles
![100 circles](https://bitbucket.org/Sufflava/d3examples/raw/master/2_Donut/results/100.png)

### 200 circles
![200 circles](https://bitbucket.org/Sufflava/d3examples/raw/master/2_Donut/results/200.png)

### 500 circles
![500 circles](https://bitbucket.org/Sufflava/d3examples/raw/master/2_Donut/results/500.png)