## Visualize Data with a Bar Chart

## Examples:

### Bar Chart
![Bar Chart](https://bitbucket.org/Sufflava/d3examples/raw/master/3_BarChart/results/1.png)

### Bar Chart with tooltip
![Bar Chart with tooltip](https://bitbucket.org/Sufflava/d3examples/raw/master/3_BarChart/results/2.png)