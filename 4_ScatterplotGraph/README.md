## Visualize Data with a Scatterplot Graph

## Examples:

### Scatterplot Graph
![Bar Chart](https://bitbucket.org/Sufflava/d3examples/raw/master/4_ScatterplotGraph/results/1.png)

### Scatterplot Graph with tooltip
![Bar Chart with tooltip](https://bitbucket.org/Sufflava/d3examples/raw/master/4_ScatterplotGraph/results/2.png)