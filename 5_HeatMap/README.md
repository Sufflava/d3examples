## Visualize Data with a Heat Map

## Examples:

### Heat Map
![Heat Map](https://bitbucket.org/Sufflava/d3examples/raw/master/5_HeatMap/results/1.png)

### Heat Map with tooltip
![Heat Map with tooltip](https://bitbucket.org/Sufflava/d3examples/raw/master/5_HeatMap/results/2.png)