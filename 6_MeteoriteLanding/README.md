## Map Data Across the Globe. Meteorite landing (zoom)

## Examples:

### Meteorite landing
![Meteorite landing](https://bitbucket.org/Sufflava/d3examples/raw/master/6_MeteoriteLanding/results/1.png)

### Meteorite landing with tooltip
![Meteorite landing with tooltip](https://bitbucket.org/Sufflava/d3examples/raw/master/6_MeteoriteLanding/results/2.png)