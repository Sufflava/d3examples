## Show National Contiguity with a Force Directed Graph

## Examples:

### National Contiguity
![National Contiguity](https://bitbucket.org/Sufflava/d3examples/raw/master/7_NationalContiguity/results/1.png)

### National Contiguity with tooltip
![Meteorite landing with tooltip](https://bitbucket.org/Sufflava/d3examples/raw/master/7_NationalContiguity/results/2.png)