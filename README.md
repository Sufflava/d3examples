## D3 examples

## 1. Draw circles around the point

![20 circles](https://bitbucket.org/Sufflava/d3examples/raw/master/1_CirclesAroundPoint/results/20.png)

## 2. Draw circles inside a donut

![200 circles](https://bitbucket.org/Sufflava/d3examples/raw/master/2_Donut/results/200.png)

## 3. Visualize Data with a Bar Chart

![Bar Chart](https://bitbucket.org/Sufflava/d3examples/raw/master/3_BarChart/results/2.png)

## 4. Visualize Data with a Scatterplot Graph

![Scatterplot Graph](https://bitbucket.org/Sufflava/d3examples/raw/master/4_ScatterplotGraph/results/2.png)

## 5. Visualize Data with a Heat Map

![Heat Map](https://bitbucket.org/Sufflava/d3examples/raw/master/5_HeatMap/results/1.png)

## 6. Map Data Across the Globe. Meteorite landing (zoom)

![Meteorite landing](https://bitbucket.org/Sufflava/d3examples/raw/master/6_MeteoriteLanding/results/1.png)

## 7. Show National Contiguity with a Force Directed Graph

![National Contiguity](https://bitbucket.org/Sufflava/d3examples/raw/master/7_NationalContiguity/results/1.png)